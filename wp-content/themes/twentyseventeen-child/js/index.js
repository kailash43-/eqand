(function($){

  $(document).ready(function(){

	$('.menu-button i').click(function(){
		$('.menu-content').slideToggle("fast");
	})


    // Basic Functions
    //––––––––––––––––––
    function windowWidth() {
        var winWidth = $(window).width();
        // console.log(winWidth);
        return winWidth;
    };

    function windowHeight() {
        var winHeight = $(window).height();
        // console.log(winHeight);
        return winHeight;
    };

    // Slider Init
    //––––––––––––––––––––
    $('#main-slider .slide').css('height', windowHeight());

    // var slider = new Slider();
    $('#main-slider').Slider({
        speed: 800,
        mode: 'horizontal',
    });

    //Auto slide
    // setInterval(function () {
    //     $('.arrow-right').trigger('click');
    // }, 6000);

    // Functions onResize
    //––––––––––––––––––––
    $(window).resize(function() {
        $('#main-slider .slide').css('height', windowHeight());
    });

	// var fixmeTop = $('.fixme').offset().top;
	// $(window).scroll(function() {
	//     var currentScroll = $(window).scrollTop();
	//     if (currentScroll >= fixmeTop-144) {
	//         $('.logo').css({
	//             position: 'fixed',
	//             top: '0',
	//             left: '0',
	//             background: 'rgba(0,0,0,0.1)'
	//         });
	//         $('.menu-button i').css({
	//             position: 'fixed',
	//             top: '25px',
	//             right: '10px'
	//         });
	//          $('.menu-content').css({
	//             top: '60px',
	//             right: '10px'
	//         });
	//     } else {
	//         $('.logo').css({
	//             position: 'static',
	//             background: 'rgba(255,255,255,0.5)'
	//         });
	//         $('.menu-button i').css({
	//             position: 'static'
	//         });
	//         $('.menu-content').css({
	//             top: '80px',
	//             right: '16px'
	//         });
	// 		$('.menu-content').hide();	
	//     }
	// });

	$('.write_story').click(function() {
		alert();
	})


	  // <?php
   //  // TO SHOW THE POST CONTENTS
   //  ?>                        
   //      <?php
   //      $my_query = new WP_Query( 'cat=1' ); // I used a category id 1 as an example
   //      ?>
   //      <?php if ( $my_query->have_posts() ) : ?>
   //      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   //      <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>

   //          <h1 class="entry-title"><?php the_title(); ?></h1> <!-- Queried Post Title -->
   //          <div class="entry-content">
   //              <?php the_excerpt(); ?> <!-- Queried Post Excerpts -->
   //          </div><!-- .entry-content -->

   //      <?php endwhile; //resetting the post loop ?>

   //      </div><!-- #post-<?php the_ID(); ?> -->

   //      <?php
   //      wp_reset_postdata(); //resetting the post query
   //      endif;
   //      ?>

});
})(jQuery);