<?php
/*
Template Name: About Us
*/

get_header();
?>
<div class="about_us">
	<div class="container text-left">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
			<?php
				// Start the loop.
				while ( have_posts() ) : the_post();?>

					<h1 class="page_title text-center">
						<?php the_title(); ?>
					</h1>
						</div>
		</div>
	</div>
		<div class="row margin-zero">
			<div class="col-md-12 col-lg-12 col-sm-12 padding-zero">
					<div class="about_feature_image">
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
						<img src="<?php echo $thumb['0'];?>" alt="">
				</div>
		</div>	</div>

	<div class="container text-left">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
					<div class="about_content"> <?php the_content(); ?> </div>
		          
		          <?php  // Include the page content template.
					//get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}

					// End of the loop.
				endwhile;
				?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>