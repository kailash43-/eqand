<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="row footer margin-zero">
				<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>/">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/logo.png" alt="Smiley face" height="auto" width="200px">
				</a>
				<div class="row margin-zero">
					<div class="social-icons">
				    	 	<span><a href=""><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></span>
				    	 	<span><a href=""><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></span>
				    	 	<span><a href=""><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></span>
				    	 	<span><a href=""><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a></span>
				    	 </div>
				</div>
				<div class="row margin-zero footer-menu">
					<?php wp_nav_menu( array( 'theme_location' => 'MainMenu') ); ?>
				</div>
			</div><!-- .row -->
			<div class="row margin-zero copy-right">
				<span>Copyright &copy; 2017 EQand. All rights reserved.</span>
			</div>
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
</div>
<?php wp_footer(); ?>

</body>

<!--<script type='text/javascript' src='<?php bloginfo( 'template_url' ); ?>/js/k.min.js'></script>-->
</html>
