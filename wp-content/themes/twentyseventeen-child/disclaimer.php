<?php
/*
Template Name: Disclaimer
*/

get_header();
?>
<div>
	Disclaimer
<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			the_content();
            // Include the page content template.
			//get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

</div>


<?php get_footer(); ?>