<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="home-page">
	<div id="main-slider">
	<?php
		$loop = new WP_Query( array('post_type'=> 'post', 'posts_per_page'=>'5','post__not_in' => array(84) ) ); 
		$firstPosts = array();
	?>
	<?php while ( $loop->have_posts() )  : $loop->the_post();?>
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
		<div class="slide slide-1" style="background-image: url('<?php echo $thumb['0'];?>')">
	        <div class="container">
	            <div class="vertical-hook"></div>
	            <a href="<?php the_permalink(); ?>">
		            <div class="vertical-container">
		                <header>
				                <h1>
									<?php the_title(); ?>
								
								</h1>
								<!-- <?php $content = get_the_content(); echo mb_strimwidth($content, 0, 360, '...');?> -->
		                </header>

		                <button type="button" class="btn btn-default btn-lg write_story">
							KNOW MORE
						</button>
		            </div>
	            </a>
	        </div>
	    </div>

	    <?php endwhile; ?>
		<?php 
			$loop_2 = new WP_Query( array('post__not_in' => $firstPosts ) );
			while ( $loop_2->have_posts() )  : $loop_2->the_post();
			$thumb_2 = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )
		?>
		<?php if ( $post->ID == '84' ) { ?>
			<div class="slide slide-1" style="background-image: url('<?php echo $thumb_2['0'];?>')">
		        <div class="container">
		            <div class="vertical-hook"></div>
		            <a href="<?php the_permalink(); ?>">
			            <div class="vertical-container">
			                <header>
					                <h1>
										<?php the_title(); ?>
									
									</h1>
									<!-- <?php $content = get_the_content(); echo mb_strimwidth($content, 0, 360, '...');?> -->
			                </header>

			                <button type="button" class="btn btn-default btn-lg write_story">
								KNOW MORE
							</button>
			            </div>
		            </a>
		        </div>
		    </div>
	    <?php } ?>

		<?php endwhile; ?>
	    <?php wp_reset_query(); ?>
	</div>
</div>
<!-- <div class="row margin-zero second-container margin-zero">
		<div class="style_prevu_kit_second" style="background-color:#fda400;">
			<h5>Write<br>your story</h5>
			<p>Tell your stories and <br> inspire others like you!</p>
			<button type="button" class="btn btn-default btn-lg write_story">
				Write a story
			</button>
		</div>
</div> -->
<div class="row upcoming_events_container margin-zero">

	<div class="container">
		<div class="col-md-12">
			<h2>Upcoming Events</h2>
		</div>
		<?php $loop = new WP_Query( array('post_type'=> 'upcoming-events', 'posts_per_page'=>'1') ); ?>
		<div class="row">
			
			<div class="col-md-5 padding-zero text-left event-right-container">
			<?php  while ( $loop->have_posts() )  : $loop->the_post(); ?>
				<a href="<?php the_permalink(); ?>">
				<h5><?php the_title(); ?></h5>		
				<div class="postContent">
					<?php $content = get_the_content(); echo mb_strimwidth($content, 0, 200, '...') ?>
				</div>
				</a>
			</div>
			
			<div class="col-md-7 text-left event-left-container">
				<div class="row event-left-content">
					<div class="col-md-5 offset-md-1 padding-zero event_day_location">
						<p class="tp_pst_dt"><?php the_time('l, j M Y') ?></p>
						 <p class="event_timing"> 
				    		<?php $custom = get_post_custom(); $key_name = get_post_custom_values($key = 'Event time'); echo $key_name[0];?></p>
				    		<p class="event_location">
				    		<?php $custom = get_post_custom(); $key_name = get_post_custom_values($key = 'Event Location'); echo $key_name[0];?></p>

					</div>
					<div class="col-md-5 offset-md-1 event_day_location">
						<p>Contact for more details</p>
						 <p class="event_timing"> 
				    		<?php $custom = get_post_custom(); $key_name = get_post_custom_values($key = 'Event contact number'); echo $key_name[0];?></p>
				    	<p class="event_location">
				    		<?php $custom = get_post_custom(); $key_name = get_post_custom_values($key = 'Event contact email'); echo $key_name[0];?></p>
					</div>
				</div>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</div>
</div>
<div class="row advertisement_container margin-zero">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-12">
			<div class="print advertisement_content">
				AD1
			</div>
				
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12">
			<div class="print advertisement_content">
				AD1
			</div>
				
			</div>
		</div>
		<div class="row">
			<button type="button" class="btn btn-default btn-lg add_contact">
				TO SHOW ADD HERE KNOW MORE
			</button>
		</div>
	</div>
</div>
<div class="row subscribe_container margin-zero">
<div class="container">
	<div class="row margin-zero">
	<div class="col-md-8 padding-zero">
		<div class="row">
			<div class="col-md-5 padding-zero">
				<input type="text" placeholder="Enter your name">
			</div>
			<div class="col-md-5 padding-zero">
				<input type="email" placeholder="Enter your email id">
			</div>
			<div class="col-md-2 padding-zero">
				<button type="button" class="btn btn-default btn-lg subscribe_button">
					SUBSCRIBE
				</button>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<button type="button" class="btn btn-default btn-lg write_story">
				Write a story
			</button>
	</div>
	</div>
</div>
</div>
<div class="row margin-zero third-container">
	
		<div class="col-md-12 title-heading">
			<h2>EQ Articles</h2>
		</div>
		<?php $loop = new WP_Query( array('post_type'=> 'post', 'posts_per_page'=>'4') ); ?>
		<div class="row article-container">
			<?php while ( $loop->have_posts() )  : $loop->the_post();?>
			<div class="col-md-3 article-content"> 
				<img class="image" src="<?php bloginfo( 'template_url' ); ?>/images/pic-1.jpg" alt="old man" height="450" width="345">
				 <div class="overlay">
				    <div class="text">
				    	 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				    	 <div class="custom_read_time">
				    	 <span class="time"><?php the_time('M j, Y') ?> - </span>
				    	 <span class="read_time"> 
				    		<?php $custom = get_post_custom(); $key_name = get_post_custom_values($key = 'Read Time'); echo $key_name[0];?></span>
							</div>
				    	 <div class="line-symbol"></div>
				    	 <div class="social-icons">
				    	 	<span><a href=""><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></span>
				    	 	<span><a href=""><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></span>
				    	 	<span><a href=""><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></span>
				    	 	<span><a href=""><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a></span>
				    	 </div>

				    </div>
				  </div>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	<!-- 	<div class="col-md-12">
			<button type="button" class="btn btn-default btn-lg write_story">
				VIEW MORE
			</button>
		</div> -->

</div>
<!-- <div class="row margin-zero">
	<div class="notes-height">
		<div class="style_prevu_kit" style="background-color:#cb2025;">
			<h1>21</h1>
		</div>
		<div class="style_prevu_kit" style="background-color:#f8b334;"></div>
		<div class="style_prevu_kit" style="background-color:#97bf0d;"></div>
		<div class="style_prevu_kit" style="background-color:#00a096;"></div>
		<div class="style_prevu_kit" style="background-color:#93a6a8;"></div>  
	</div>
</div>
<div class="row margin-zero">
	<div class="advertisement-container container">
		
			<h2 class="text-center">Advertisement</h2>
			<div class="row">
			<div class="col-md-6">1</div>
			<div class="col-md-6">2</div>
		</div>
	</div>
</div>	
<div class="founder-container">
	
		<div class="row">
		<div class="col-md-6">1</div>
		<div class="col-md-6">
			<div class="row">
				<h2>founder content</h2>
			</div>
			<button type="button" class="btn btn-default btn-lg write_story">
					VIEW MORE
			</button>
		</div>
	</div>
</div>
<div class="row margin-zero">
	<div class="footer-container">
		
			<div class="row">
			<div class="col-md-8">1</div>
			<div class="col-md-4">
				<button type="button" class="btn btn-default btn-lg write_story">
					Write a story
				</button>
			</div>
		</div>
	</div>
</div> -->
		
</div>
<?php get_footer();
