<?php
/**
 * @package Twenty Seventeen Child Theme
 */

/**
 * Load the parent and child theme styles
 */
	
	function my_theme_enqueue_styles() { 
		wp_enqueue_style( 'font-awesome-style', get_stylesheet_directory_uri(). '/font-awesome/css/font-awesome.min.css' );
 		wp_enqueue_style( 'tether-style', get_stylesheet_directory_uri() . '/css/tether.min.css' );
 		wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri(). '/css/bootstrap.min.css' );
		wp_enqueue_style( 'slider-style', get_stylesheet_directory_uri(). '/css/demo.css' );
		wp_enqueue_style( 'custom-style', get_stylesheet_directory_uri(). '/css/normalize.css' );
		wp_enqueue_style( 'app-style', get_stylesheet_directory_uri(). '/css/app.css' );
		wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css' );
 	}

	add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
	
	
	function my_theme_enqueue_scripts() {
		wp_enqueue_script('jquery-script', get_stylesheet_directory_uri() . '/js/jquery.min.js');
		wp_enqueue_script('tether-script', get_stylesheet_directory_uri() . '/js/tether.min.js');
		wp_enqueue_script('bootstrap-script', get_stylesheet_directory_uri() . '/js/bootstrap.min.js');
		wp_enqueue_script('slider-script', get_stylesheet_directory_uri() . '/js/slider.js');
		wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/index.js');
	}

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'twentysixteen' ),
		'social'  => __( 'Social Links Menu', 'twentysixteen' ),
		'Followus' => __( 'Follow us', 'twentysixteen' ),
		'About' => __( 'About', 'twentysixteen' ),
		'HomeMenu' => __( 'Home Menu', 'twentysixteen' ),
		'InnerPageMenu' => __( 'Inner Page Menu', 'twentysixteen' )
	) );

	add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts' );
?>