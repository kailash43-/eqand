<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen_Child
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/images/EQandFavIco.png"> 
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="container-fluid">
	<div class="row margin-zero header-navbar">
		<div class="col-md-6 logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>/">
				<img src="<?php bloginfo( 'template_url' ); ?>/images/logo.png" alt="Smiley face" height="auto" width="200px">
			</a>
		</div>
		<div class="col-md-6 menu-button">
			<i class="fa fa-bars fa-2x" aria-hidden="true"></i>

			<div class="menu-content">
				<?php wp_nav_menu( array( 'theme_location' => 'top') ); ?>
			</div>
		</div>
</div>

