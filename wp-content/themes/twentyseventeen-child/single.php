<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="container">
			
					
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();?>

	<div class="post_content text-left">
		<h3><?php the_title(); ?></h3>
		<?php $category = get_the_category( $custompost ); echo $category[0]->cat_name;?>
		<span class="tp_pst_dt"><?php the_time('M j, Y') ?></span>
		<div class="postContent"> <?php the_content(); ?> </div>


		<?php	get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			if ( is_singular( 'attachment' ) ) {
				// Parent post navigation.
				the_post_navigation( array(
					'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen' ),
				) );
			} elseif ( is_singular( 'post' ) ) {
				// Previous/next post navigation.
				the_post_navigation( array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentysixteen' ) . '</span> ' .
						'<span class="post-title">%title</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentysixteen' ) . '</span> ' .
						'<span class="post-title">%title</span>',
				) );
			} 
			

		
			// End of the loop.
		endwhile;
		?>
		</div>
	</div>
	</main><!-- .site-main -->



</div><!-- .content-area -->

<?php get_footer(); ?>
