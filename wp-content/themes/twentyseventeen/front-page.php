<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
		<div class="row first-container">
			<?php $loop = new WP_Query( array('post_type'=> 'post', 'posts_per_page'=>'5') ); ?>
			<ul class="cb-slideshow">
				<?php while ( $loop->have_posts() )  : $loop->the_post();?>
					<li>
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
						<span style="background-image: url('<?php echo $thumb['0'];?>')"></span>
						<div>
							<h3>
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>
								</a>
							</h3>
						</div>
					</li>
				<?php endwhile; wp_reset_query(); ?>
			</ul>

			<header class='main-header-container'>
				<div class="row">
					<div class="col-md-2 logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>/">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/logo.png" alt="Smiley face" height="auto" width="200px">
						</a>
					</div>
					<div class="col-md-9">

					</div>
					<div class="col-md-1 menu-button">
						<i class="fa fa-bars fa-2x" aria-hidden="true"></i>

						<div class="menu-content">
							<?php wp_nav_menu( array( 'theme_location' => 'MainMenu') ); ?>
						</div>
					</div>
				</div>
				
			</header>
		</div>
		<div class="site-content-contain">
			<div id="content" class="site-content" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/images/conatiner-cover.jpg');">
				
</div><!-- #primary -->

<?php get_footer();
