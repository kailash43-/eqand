<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eqandDB');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R9-gi.G,^FB*._&yr.<k^:}S4OQ2PxdX{]eG_jEJda+<PQk.F5MPshXk9_/14b!%');
define('SECURE_AUTH_KEY',  'Z(<|tm*|C`n3ZGvGyH>-H1Pjm(*9&2AjPXW+Q+yD0*(;%W;lK~nPP2uZs|d~Y!fA');
define('LOGGED_IN_KEY',    'Gc>+<XA)VRqaow$#guXGH!rjf[TYwjet.(dG5zrtbCWP1lnyfk;t8DXJCSer9JM`');
define('NONCE_KEY',        '$ ;p@{<Uh~jl$|VYdM|V+&m8Rt^ggC C(Q!IFF,ezz[*u1*bwZFCcm4s#y^H9h s');
define('AUTH_SALT',        'R-yby%{Jk)q5eE_?<ZznYHz8:,EiE1X`KM-L/T-([wWQ14*|{@JSdN+Q V(Gj~@+');
define('SECURE_AUTH_SALT', ';&ej[&AvD{H|WkLpx|]K&ihRHXZn;[mp/uX#v9P^Z/~gOH)3teh$79>w~8{c@Q*5');
define('LOGGED_IN_SALT',   'Ys5fV4yqCgAEV%N7CIK0T2x[)MC==S*>&qa#_yJW:gRbS^*pb:j{@/n/DhVJEVVJ');
define('NONCE_SALT',       'aM2[BKYOWH@(L!PH>2!7EiE*wy((Ldu3tF-s~d1]_(GGUc,w.|}NMYwLXdwDlcGc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
